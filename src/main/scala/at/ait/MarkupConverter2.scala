package at.ait

import java.io.{BufferedReader, FileReader, PrintWriter}
import org.jsoup.nodes.Node


case class MarkupConverter2(split: Boolean) extends MarkupConverter {
  
  val cssSelector = "p"
    
  val csvFirstLine = "toponym;" + {if (split) "gdoc_part;" else ""} + "offset;gazetteer_id;gazetteer_uri;reason;status"
  
  val gazetteerIdMap = createGazetteerIdMap
    
  def text(child: Node, csvOutput: PrintWriter, offset: Int): String = {
    if (child.nodeName == "br") "\n"
    else if (child.nodeName == "place" ||
        child.nodeName == "ethnic" ||
        child.nodeName == "demonym") {
      val text = child.childNode(0).toString.trim
      val customId = child attr "id"
      val gazetteerIdOption = gazetteerIdMap get customId
      var reason = ""
      val gazetteerUri =
        if (gazetteerIdOption.isEmpty) {
          reason = "entry completely missing in dictionary"
          ""
        }
        else if (gazetteerIdOption.get == "none") ""
        else "http://pleiades.stoa.org/places/" + gazetteerIdOption.get
      csvOutput.write(text + ";" +
          {if (split) part + ";" else ""} +
          offset + ";" +
          customId + ";" +
          gazetteerUri + ";" +
          reason + ";")
      if (!gazetteerUri.isEmpty()) csvOutput.write("VERIFIED")
      csvOutput.write("\n")
      text
    }
    else if (child.nodeName != "#text") {
      if (child.childNodes.size != 1 || child.childNode(0).nodeName != "#text")
        println("Warning: nested tags (" + child.childNodes.size + ")" + child.toString)
      child.childNode(0).toString.trim
    }
    else child.toString
  }
  
  override def chapter(node: Node): String = {
      val code = node attr "loc"
      if (code contains '.')
        code.substring(0, code indexOf '.')
      else
        code
    }
    
  def createGazetteerIdMap: Map[String, String] = {
    val dict = new BufferedReader(new FileReader("ToposTextPleiades20140812.txt"))
    var line = dict.readLine()
    var idMap: Map[String, String] = Map()
    while (line != null) {
      val pair = line.split("\t")
      idMap += (pair(0) -> pair(1))
      line = dict.readLine()
    }
    dict.close()
    idMap
  }
}