package at.ait

import java.io.File
import java.io.PrintWriter
import org.jsoup.Jsoup
import org.jsoup.nodes.Node
import scala.collection.JavaConversions._
import org.jsoup.nodes.Entities.EscapeMode
import org.jsoup.nodes.Entities

object MarkupConverter extends App {
  val split = false
  val markupConverter = MarkupConverter2(split)
  val sourceFileBaseName = "G403StepByz"
  //val sourceFileBaseName = "catalog_of_ships"
  val input = new File(sourceFileBaseName + ".html")
  val doc = Jsoup.parse(input, "UTF-8")
  doc.outputSettings().escapeMode(EscapeMode.xhtml)
  val elements = doc.select(markupConverter.cssSelector)
  println("number of top-level elements: " + elements.size)
  
  val firstPartFilename = "output.txt"
  val csvFilename = "output.csv"
  val directory = new File(sourceFileBaseName)
  directory.mkdirs()
  var output = new PrintWriter(sourceFileBaseName + File.separator + firstPartFilename, "UTF-8")
  val csvOutput = new PrintWriter(sourceFileBaseName + File.separator + csvFilename, "UTF-8")
  val jsonOutput = new PrintWriter(sourceFileBaseName + File.separator + "output.json", "UTF-8")
  csvOutput.write(markupConverter.csvFirstLine + "\n")
  jsonOutput.write("""{
      "title":"""" + sourceFileBaseName + """",
      "annotations":"""" + csvFilename + "\"," +
      {
        if (split)
          """"parts" : [ """ + chapterAsJson(markupConverter.part, firstPartFilename)
        else
          """"text" : """" + firstPartFilename + "\""
      }
  )
  
  var resultText = ""
  var currentChapter = ""
  for (textElement <- elements) {
    val nextChapter = markupConverter.chapter(textElement)
    //if (split && !currentChapter.isEmpty /*&& !nextChapter.isEmpty*/ && nextChapter != currentChapter) {
    if (split && !currentChapter.isEmpty && !nextChapter.isEmpty && nextChapter != currentChapter) {
      println("new Chapter: " + nextChapter)
      markupConverter.partNumber += 1;
      output.write(resultText)
      output.close()
      val filename = "output_" + markupConverter.partNumber + ".txt"
      jsonOutput.write("," + chapterAsJson(markupConverter.part, filename))
      output = new PrintWriter(sourceFileBaseName + File.separator + filename, "UTF-8")
      resultText = ""
    }
    currentChapter = nextChapter
    val children = textElement.childNodes()
    for (child <- children) {
      resultText += markupConverter.text(child, csvOutput, resultText.size)
          .replaceAll("&lt;", "<")
          .replaceAll("&gt;", ">")
          .replaceAll("&amp;", "&")
          .replaceAll("&apos;", "'")
          .replaceAll("&quot;", "\"")
    }
    resultText += "\n\n"
  }
  
  output.write(resultText)
  if (split) jsonOutput.write("]")
  jsonOutput.write("}")
  csvOutput.close()
  jsonOutput.close()
  output.close()
  
  def chapterAsJson(title: String, filename: String) =
    """{
      "title" : """" + title + """",
      "text" : """" + filename + """"
    }"""
}

abstract class MarkupConverter {
  val cssSelector: String
  val csvFirstLine: String
  var partNumber = 1
  def part = "Part " + partNumber
  def text(child: Node, csvOutput: PrintWriter, offset: Int): String
  def chapter(node: Node): String = "1"
}