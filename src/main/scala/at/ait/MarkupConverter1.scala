package at.ait

import java.io.PrintWriter
import org.jsoup.nodes.Node

object MarkupConverter1 extends MarkupConverter {
  
  val cssSelector = ".element-text"
    
  val csvFirstLine = "toponym;offset;gazetteer_uri;status"
  
  def text(child: Node, csvOutput: PrintWriter, offset: Int) : String =
    if (child.nodeName() == "br") "\n"
    else if (child.nodeName() == "span") {
      val text = child.childNode(0).toString
      if (child.attr("class").contains("neatline-span")) {
        csvOutput.write(text + ";" +
            offset + ";" +
            child.attr("data-pleiades-uri") + ";")
        if (!child.attr("data-pleiades-uri").isEmpty())
          csvOutput.write("VERIFIED")
        csvOutput.write("\n")
      }
      text
    }
    else child.toString

}